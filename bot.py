#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# TODO:
# Освоить потоки
# Что-то ещё, но я пока не знаю

import socket
import sys
import os
import configparser
import requests
import codecs
import re
import html
import logging
from hurry.filesize import size
from contextlib import closing

IsSelfChecked = False
IsTheBotAnAdmin = False

class IRC:
    ipv4 = None
    irc = None

    def __init__(self):
        self.irc = socket.socket()

    def send(self, chan, msg):
        self.irc.send(bytes("PRIVMSG %s :%s\n" % (chan, msg), "UTF-8"))

    def connect(self, server, channel, port, nick, passw, np, user):
        print("[*] Connecting to " + server)
        print("[*] The nick is " + nick)
        try:
            socket.getaddrinfo(server, None)
            self.ipv4 = True
        except socket.gaierror:
            pass
            self.ipv4 = False
        if self.ipv4:
            self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.irc = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        self.irc.connect((server, port))
        self.irc.send(bytes("nick %s\r\n" % user, "UTF-8"))
        self.irc.send(bytes("user %s 0 * :%s\r\n" % (nick, nick), "UTF-8"))
        if np:
            self.irc.send(bytes("PRIVMSG nickSERV :identify %s\r\n" % passw, "UTF-8"))
        self.irc.send(bytes("JOIN %s\r\n" % channel, "UTF-8"))

    def self_test(self, nick, text):
        global IsSelfChecked, IsTheBotAnAdmin
        if "@" + nick not in self.get_nick(text):
            print(
                "[!] The bot is not an operator. It is recommended to make bot an operator."
            )
        else:
            print("[*] The bot is an operator")
            IsTheBotAnAdmin = True
        IsSelfChecked = True

    def get_text(self):
        text = self.irc.recv(4096).decode("UTF-8")

        return text.replace("\r\n", "")

    def kick(self, channel, nick, msg):
        if msg is not None:
            self.irc.send(bytes("KICK %s %s %s\r\n" % (channel, nick, msg), "UTF-8"))
        else:
            self.irc.send(bytes("KICK %s %s\r\n" % (channel, nick), "UTF-8"))

    def pong(self, pong):
        self.irc.send(bytes(" PONG %s\r\n" % pong, "UTF-8"))

    def exit(self, msg):
        self.irc.send(bytes("QUIT :%s\r\n" % msg, "UTF-8"))
        sys.exit("[*] Exiting...")

    def give_op(self, nick):
        self.irc.send(bytes("MODE %s +o" % nick, "UTF-8"))

    def get_nick(self, text):
        return (
            str(text)
                .split("!")[0]
                .replace(":", "")
                .replace(" ", "")
                .replace("[", "")
                .replace("]", "")
                .replace("'", "")
        )

    def get_nick_and_msg(self, channel, text):
        nick = (
            str(text)
                .split("!")[0]
                .replace(":", "")
                .replace(" ", "")
                .replace("[", "")
                .replace("]", "")
                .replace("'", "")
        )
        ip = (
            str(text)
                .split("@")[1]
                .replace(":", "")
                .replace("[", "")
                .replace("]", "")
                .replace("'", "")
                .split(" ")[0]
        )
        message = None
        if len(str(text).split(channel + " :")) > 1:
            message = str(text).split(channel + " :")[1]
        return [ip, nick, message]

    def check_if_user_is_op(self, text, users):
        if self.get_nick(text) in users:
            self.give_op(self.get_nick(text))

    def send_notice(self, chan, msg):
        self.irc.send(bytes("NOTICE %s :%s\n" % (chan, msg), "UTF-8"))


class ConnectionParams(object):
    def __init__(self, server_name, port, nick, user, password, channel, needed_pass):
        self.server_name = server_name
        self.port = port
        self.nick = nick
        self.user = user
        self.password = password
        self.channel = channel
        self.needed_password = needed_pass


class OtherCommands():
    def show_link_title(self, link_, limit):
        r = None
        try:
            retitle = re.compile("<title[^>]*>(.*?)</title>", re.IGNORECASE | re.DOTALL)
            buffer = ""
            with closing(requests.get(link_, stream=True)) as res:
                if (
                        "text/html" in res.headers.get("content-type")
                        and len(res.content) < limit
                ):
                    for chunk in res.iter_content(chunk_size=1024, decode_unicode=True):
                        buffer = "".join([buffer, chunk])
                        match = retitle.search(buffer)
                        if match:
                            return (
                                html.unescape(match.group(1))
                                    .replace("\n", "")
                                    .replace("\r", "")
                            )
                elif (
                        "text/html" in res.headers.get("content-type")
                        and len(res.content) > limit
                ):
                    return "The webpage size is too long"
                else:
                    return (
                            "Not a webpage: "
                            + res.headers.get("content-type")
                            + " "
                            + size(int(res.headers.get("content-length")))
                    )
        except requests.exceptions.ConnectionError:
            return "Connection refused"  # Божественный костыль
        finally:
            if r is not None:
                r.close()

    def restart(self):
        print("[*] Restarting...")
        os.execv(sys.executable, ["python"] + sys.argv)


class Logging:
    logger = None
    path = None
    log_std = True
    log_file = True
    log_formatter = None
    file_handler = None
    stdout_handler = None

    def init_log(self):
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        self.log_formatter = logging.Formatter("%(asctime)s %(message)s")

        if self.log_std is True:
            self.stdout_handler = logging.StreamHandler(sys.stdout)
            self.stdout_handler.setFormatter(self.log_formatter)
            self.logger.addHandler(self.stdout_handler)

        if self.log_file is True:
            self.file_handler = logging.FileHandler(str(self.path), encoding="UTF-8")
            self.file_handler.setFormatter(self.log_formatter)
            self.logger.addHandler(self.file_handler)

    def log(self, msg):
        self.logger.info(msg)

    def __init__(self, path, log_std, log_file):
        self.path = path
        self.log_std = log_std
        self.log_file = log_file
        self.init_log()


def main():
    global IsSelfChecked, IsTheBotAnAdmin
    is_entered = False
    cfg = configparser.RawConfigParser()

    if len(sys.argv) > 1:
        with codecs.open(sys.argv[1], "r", encoding="utf-8", errors="ignore") as f:
            cfg.read_file(f)
    else:
        print("[!] You need to specify config file!")

    conn_params = ConnectionParams(cfg.get("Main", "ServerName").strip('"'), cfg.get("Main", "Port").strip('"'),
                                   cfg.get("Main", "Nick").strip('"'), cfg.get("Main", "User").strip('"'),
                                   cfg.get("Main", "Password").strip('"'), "#" + cfg.get("Main", "Channel").strip('"'),
                                   cfg.getboolean("Main", "NeededPassword"))

    kick_message = cfg.get("Messages", "KickMessage").strip(
        '"'
    )  # По какому сообщению бот будет кикать
    url_message = cfg.get("Messages", "LinkMessage").strip(
        '"'
    )  # Что бот будет писать перед заголовком
    kill_message = cfg.get("Messages", "KillMessage").strip(
        '"'
    )  # Сообщение, при котором бот будет прекращать свою работу
    kick_reason_needed = cfg.getboolean("Messages", "KickReasonNeeded")
    quit_message = cfg.get("Messages", "QuitMessage").strip('"')
    restart_message = cfg.get("Messages", "RestartMessage").strip('"')
    not_owner_msg = cfg.get("Messages", "NotOwner").strip('"')

    op_users = cfg.get("Users", "PrivilegedUsers").strip(
        '"'
    )  # Кому давать ОПа при входе
    owner_users = cfg.get("Users", "Owners").strip('"')  # Владельцы этого засранца
    owners_are_pu = cfg.get("Users", "OwnersArePU")

    page_limit = cfg.get("Links", "PageLimit")

    send_links_as_notices = cfg.get("Links", "SendAsNotice")

    logging = Logging(cfg.get("Log", "LogPath").strip('"'), cfg.getboolean("Log", "LogSTDOUT"), cfg.getboolean("Log", "LogFile"))

    oc = OtherCommands()

    if kick_reason_needed is True:
        kick_reason = cfg.get("Messages", "KickReason").strip('"')  # Причина кика
    else:
        kick_reason = None

    kick_messages = cfg.get("Other", "KickMessages").replace('"', "").replace(" ", "").split(",")
    kick_for_message = cfg.getboolean("Other", "KickForMessages")

    if owners_are_pu:
        owner_users = op_users

    irc = IRC()
    irc.connect(conn_params.server_name, conn_params.channel, int(conn_params.port), conn_params.nick,
                conn_params.password, conn_params.needed_password, conn_params.user)

    # logger = logging.getLogger()

    while True:
        try:
            text = irc.get_text()
            if conn_params.channel in text:
                is_entered = True

            if text.find("PING") != -1:
                irc.pong(text.split()[1])
            if text.find("Invalid password for") != -1:
                print("[!] Invalid password")
                sys.exit("[*] Exiting...")

            if "Cannot join channel" in text:
                print("[!] Cannot join channel (%s)" % text.split("-")[1].strip(" "))
                sys.exit("[*] Exiting...")

            if is_entered:
                text_parsed_with_space = (
                    text.split("\n")[0].strip(",").strip("\r").strip(":").split(" ")
                )

                if IsSelfChecked is False:
                    irc.self_test(conn_params.nick, text)

                for part in text_parsed_with_space:
                    part = part.strip(":")

                    if (
                            "http://" in part
                            or "https://" in part
                            and conn_params.user not in text_parsed_with_space
                    ):
                        if send_links_as_notices:
                            irc.send_notice(
                                conn_params.channel,
                                url_message
                                + str(oc.show_link_title(str(part), int(page_limit))),
                            )
                        else:
                            irc.send(
                                conn_params.channel,
                                url_message
                                + str(oc.show_link_title(str(part), int(page_limit))),
                            )

                    if "KICK" in part and conn_params.user in text_parsed_with_space:
                        print("[*] The bot has been kicked from the channel")
                        sys.exit("[*] Exiting...")

                    if kick_for_message and IsTheBotAnAdmin:
                        if part.lower() in [x.lower() for x in kick_messages]:
                            irc.kick(
                                conn_params.channel,
                                irc.get_nick(text_parsed_with_space),
                                kick_reason,
                            )

                if "+o" in text_parsed_with_space and conn_params.user in text_parsed_with_space:
                    IsTheBotAnAdmin = True
                    print("[*] The bot is an operator now")

                if "-o" in text_parsed_with_space and conn_params.user in text_parsed_with_space:
                    IsTheBotAnAdmin = False
                    print("[*] The bot is not an operator now")

                if conn_params.user in text and kill_message in text_parsed_with_space:
                    if irc.get_nick(text_parsed_with_space) in owner_users:
                        irc.exit(quit_message)
                        sys.exit("[*] Exiting...")
                    else:
                        irc.send(conn_params.channel, not_owner_msg)

                if conn_params.user in text and kick_message in text_parsed_with_space:
                    if (
                            irc.get_nick(text_parsed_with_space) in owner_users
                            and IsTheBotAnAdmin
                    ):
                        irc.kick(conn_params.channel, text_parsed_with_space[5], kick_reason)
                    else:
                        irc.send(conn_params.channel, not_owner_msg)

                if conn_params.user in text and restart_message in text_parsed_with_space:
                    if irc.get_nick(text_parsed_with_space) in owner_users:
                        oc.restart()
                    else:
                        irc.send(conn_params.channel, not_owner_msg)
                if "PING" not in text:
                    logf = irc.get_nick_and_msg(conn_params.channel, text)
                if "/NAMES" not in text:
                    if "JOIN" in text:
                        logging.log("%s@%s has joined" % (logf[1], logf[0]))
                    if "PART" in text:
                        logging.log("%s@%s has left" % (logf[1], logf[0]))
                    if "PRIVMSG" in text and "ACTION" not in text:
                        logging.log("%s@%s: %s" % (logf[1], logf[0], logf[2]))
                    if "PRIVMSG" in text and "ACTION" in text:
                        logging.log(
                            "%s@%s: /me %s"
                            % (
                                logf[1],
                                logf[0],
                                text.split("ACTION")[1].strip("\x01").strip(" "),
                            )
                        )
                    if "KICK" in text:
                        logging.log(
                            "%s@%s has been kicked by %s"
                            % (irc.get_nick(text), logf[0], logf[1])
                        )
                    if "QUIT" in text:
                        logging.log("%s@%s has quit" % (logf[1], logf[0]))

        except KeyboardInterrupt:
            irc.exit(quit_message)
            sys.exit("[*] Exiting...")


if __name__ == "__main__":
    main()
