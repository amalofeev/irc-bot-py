# Простой бот для IRC на python.

Однопоточный (по экземпляру процесса на канал), поддержка IPv6, логирование канала (в разработке веб сервис), управление командами в IRC (в разработке), возвращение в канал имени url, адреса которых посланы в канал. 

Требуется `>=python3.5`, `hurry.filesize`, `logging` (`requests` для чистого debian)

В данном руководстве в блоках кода в примерах команд используется приглашение:

`$` для не привелигированного пользователя

`#` для пользователя с правами `root`

Если базовая конфигурация полностью устраивает и вы просто копируете команды, то приглашения копировать не нужно.

## Установка:

Предлагается использовать удобный менеджер пакетов для python `pip3` и `git`.

Вы можете использовать любой другой удобный вам способ.

Клонирование репозитория

```
$ git clone https://gitlab.com/amalofeev/irc-bot-py
```

Установка модулей

```
$ pip3 install hurry.filesize
$ pip3 install requests
```

Модуль `logging` на октябрь 2018 устанавливается с ошибками, но работает:

```
$ pip3 install logging
```

## Настройка

Переход в директорию

```
$ cd irc-bot-py
```

Копирование конфига из примера

```
$ cp channel.conf.example channel.conf
```

Редактирование

```
$ ee channel.conf
```

### Пример конфигурации с комментариями, комментарии внутри файла конфигурации не поддерживаются

```
[Main]
ServerName = "chat.freenode.net"        # Адрес сервера
Port = 6667                             # Порт, только non-ssl
User = "bot"                   	 	# Ник бота в сети
Nick = "user"				# Имя зарегистрированного в сети пользователя
Password = "************"		# Пароль пользователя
NeededPassword = True			# Авторизация
Channel = "channel"			# Имя канала

[Messages]
KickMessage = "выброси"			# команда выброса <выброси> <nick> <причина>
KickReason = "причина выброса"		# причина выброса, если не задано в команде
LinkMessage = "⤷ "			# заголовок для парсера url
KillMessage = "выключись"		# команда выключения
QuitMessage = "пока"			# сообшение при выходе
RestartMessage = "перезапустись"	# команда перезапуска
NotOwner = "Недостаточно прав!"		# ответ не привелигированному пользователю
KickReasonNeeded = False		# включение сообщения при выбрасывании

[Users]
PrivilegedUsers = "user"		# Привелигированный пользователь
Owners = ""				# Администратор бота
OwnersArePU = True			# Включение администрирования

[Links]
PageLimit = 60000000			# лимит размера страницы по url в байтах
SendAsNotice = True			# Подавление MSG в канал, бот отправляет NOTICE

[Other]
KickForMessages = True			# Выбрасывание пользователей по фильтру запрещенных слов
KickMessages = "ла", "ло", "лы"		# список запрещенных слов

[Log]
LogFile = True      # Логгирование в файл
LogSTDOUT = True    # Логгирование в STDOUT, позволяет смотреть лог канала в journalctl -ux irc-bot-py@конфиг
LogPath = "/path/to/channel.log"	# Лог канала, абсолютный путь к расположению файла лога
```
Для выбрасывания бот должен иметь соответствующие права на канале.

## Запуск

```
$ python3.x /path/to/dir/bot.py /path/to/dir/channel.conf
```

## Автозапуск через systemd

Копирование примера в рабочую директорию

```
# cp irc-bot-py@.service /etc/systemd/system/
```

Редактирование конфигурации

```
# ee /etc/systemd/system/irc-bot-py@.service
```

Включение юнита
```
# systemctl enable irc-bot-py@channel.conf.service
```

Запуск юнита

```
# systemctl start irc-bot-py@channel.conf.service
```

Проверка запуска

```
# systemctl status irc-bot-py@channel.conf.service
```

При возникновении проблем просмотр лога и сохранение

```
# journalctl -u irc-bot-py@channel.conf.service
# journalctl --no-pager -u irc-bot-py@channel.conf.service > bot.log
```
